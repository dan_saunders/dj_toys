# Good evening all! It turns out Sainsburies bourbon and cola is damn decent...

# This tool uses bokeh - a plotting library, suitable for custom user interactions.
# Installation details can be found here:
# http://bokeh.pydata.org/en/latest/docs/installation.html

# I recommend installing using pip (which should be fine for linux, and probably
# mac). If you're a windows user, and yet somehow a physicist, i suggest this 
# website to get started for a life changing experiance:
# https://www.ubuntu.com/

# One alternative to that would be to sell ones soul and pay twice as much as 
# required for equivilent hardware + a light-up fruit (oh yeah, it doesn't even
# light up anymore - i mean, i've heard of austerity, but FFS, even the apple
# light on the back of a mac? My 11-inch mac is super thin and still has one of 
# those... yeah, i was an apple bitch once... i should make this non-editable so
# Jonas has to leave it here. Gotta love Jonas. Love him! Damn you...).

# !! Run with: bokeh serve jonas_toy.py
# In a web browser, navigate to: localhost:5006/jonas_toy

# This example borrows heavily from: 
# https://bokeh.pydata.org/en/latest/docs/user_guide/extensions_gallery/tool.html
# Enjoy! Dan Saunders, Jonas' Bitch. 
#-----

from bokeh.plotting import figure
from bokeh import events
from bokeh.layouts import column, row
from bokeh.models import Div, Range1d, ColumnDataSource
from bokeh.io import curdoc
from bokeh.models.widgets import Button
import numpy as np 

# Input functions (each a series of xs and ys) have to be stored in a bokeh object,
# namely a ColumnDataSource object - one per function.
nFuncs = 4
sources = []
for i in range(nFuncs): sources.append(ColumnDataSource(dict(x = [], y = [])))


# Create a figure to draw on (arbitrary x and y limits that i'll just use unity here).
p = figure(x_range=Range1d(0, 1.0), y_range=Range1d(0, 1.0), plot_width=600, 
        plot_height=600, tools = [])
# I need the world to know i did an awesome vim command to change the stuff in 
# brackets in that line.


# Actaully add the functions to the plot (empty to begin with) - store the returned
# plotted object in a list.
plotted_lines = []
cols = ['blue', 'red', 'green', 'orange'] # Styling.
for i in range(nFuncs): 
    # Bokeh plots lines by using a the figure.line() method, which returns an object
    # that describes the line, allowing it to be updated at a later time.
    line = p.line('x', 'y', source = sources[i], line_color = cols[i], line_width = 1.5)
    plotted_lines.append(line)


# Callbacks for PanStart and Pan user events. We need to track the currently 
# edited function - this will be done using a global variable, which (for python
# global rules reasons) need to be put in a single length array (this allows the
# value to be edited).
ifunc = [0]

def panStart_update_function(attributes = []):
    # This callback happens when someone starts drawing a line. This means 
    # start a new function (so increment the ifunc counter), and clear whatever
    # is in the function already. For bokeh reasons, which allow the magic, this
    # function actually has to return another function (that takes an input 'event' - 
    # a bokeh object that describes the user interaction).
    def python_callback(event):
	ifunc[0] = (ifunc[0] + 1) % nFuncs
	plotted_lines[ifunc[0]].data_source.data['x'] = []
	plotted_lines[ifunc[0]].data_source.data['y'] = []

    return python_callback

def pan_update_function(attributes = []):
    # Callback for when the cursor is panning (and so therefore, after panStart
    # but before panEnd. We don't need to bother with panEnd, since that is when
    # the user releases the mouse, but i guess we could in the future). When the 
    # mouse is moved, update the function xs and ys. This is currently done for each 
    # pan event, and the precision seems very good (but not so extreme to hinder 
    # perfomance). 
    def python_callback(event):
        # For reasons i don't understand, the data sources cannot be appended, 
        # but they can be replaced - so, make a copy of the current xs and ys,
        # append to those, and then replace the data sources (i'm sure someone
        # who is used to C++ will turn in their grave at the following code,
        # but life it too short to be concerned with such things).

        # Python tip: the list keyword here forces python to make a copy of the 
        # lists, unlike make a pointer (which it would do by default without this
        # keyword). 
	old_xs = list(plotted_lines[ifunc[0]].data_source.data['x'])
	old_ys = list(plotted_lines[ifunc[0]].data_source.data['y'])

	old_xs.append(event.__dict__['x'])
	old_ys.append(event.__dict__['y'])
	
	plotted_lines[ifunc[0]].data_source.data['x']  = old_xs
	plotted_lines[ifunc[0]].data_source.data['y'] = old_ys
    
    return python_callback


point_attributes = ['x','y','sx','sy']
p.on_event(events.PanStart, panStart_update_function(attributes = point_attributes))
p.on_event(events.Pan, pan_update_function(attributes = point_attributes))


#-------------- Stuff for the hist and convolving -----------------------------

pHist = figure(x_range=Range1d(0, 1.0), plot_width=600, 
        plot_height=600, tools = [])
histData = ColumnDataSource(dict(x_low = [], x_high = [], y = []))
histGlyph = pHist.quad(top='y', bottom=0, left='x_low', right='x_high',
        fill_color="#1f77b4", source = histData, line_width = 0.5, line_color = 'black')

def yfunc(fys, fxs, x_try):
    # Return the value of the function defined by the points fys and fxs at x_try.
    # In this implimentation, just loop over the input points and take a point
    # close enough.
    nPoints = len(fxs)
    min_diff = abs(fxs[0] - x_try)
    y_closest = fys[0]
    for iPoint in range(1, nPoints):
        # Find the difference between the functions x and the x we need to eval.
        # Return the y of the closest point.
        diff = abs(fxs[iPoint] - x_try)
        if diff < min_diff:
            y_closest = fys[iPoint]
            min_diff = diff
        
    return y_closest

def get_sample(iF):
    # Return a sample for the iF function. To do this, we use the 'dart-throwing'
    # method (ask Jonas for further details). In summary, we throw a set of 
    # darts at a 2D box that encapsulates the function. If the dart is also 
    # in the region of the function, return the x as the generated number. 
    # We keep trying until a number is returned (no protections for silly functions).
    
    fys = plotted_lines[iF].data_source.data['y'] 
    fxs = plotted_lines[iF].data_source.data['x'] 

    # The encapsulating box is as wide as the defined function, and the height
    # does from zero to the max y (i guess we could do something clever using min(y))
    # - bonus marks for the student who figures that out.
    ymax = max(fys)
    xmax = max(fxs)
    xmin = min(fxs)

    y_try, y_try_func = 2, 1 # Arbitrary.
    while y_try > y_try_func: # Always true the first time by construction.
        x_try = np.random.random() * (xmax - xmin) + xmin
        y_try = np.random.random() * ymax
        y_try_func = yfunc(fys, fxs, x_try)
    
    return x_try # I bet the scope of x_try annoys someone else who reads this too.


# Callback for the convole button. This has to do the following:
# - Generate a fixed number of events for each function.
# - Sum them up (but in reality, average them).
# - Histogram the summed values.
# - Update the hist figure data source to show this new hist (replacing the old).
def convolve():
    def python_callback(event):
        # The following line was used just for a test (it generates 10k numbers
        # according to a gaus dist and does some scaling) - wouldn't it be 
        # hilarious if i just left this line uncommented?
        #xs = np.random.randn(10000)/10. + 0.5 

        xs = [] # Container for the values to histogram.
        nSamples = 1000 # Num events to generate for histogram.
        for iS in range(nSamples): 
            # Get the events. get_sample(iF) returns a number sampled from the iF
            # function. This uses list comprehension to put those samples into
            # and list, and then we take the average of that list.
            # Only do this for functions that have some points to define itself.
            x = [get_sample(iF) for iF in range(nFuncs) if len(plotted_lines[iF].data_source.data['x']) > 1]
            # --> Kostas was right - comprehensions are damn sexy...
            xs.append(np.average(x))

        # Use numpy to do the histograming, then pass the bin heights and edges
        # to the bokeh plotting function.
        bin_heights, bin_edges = np.histogram(xs, range = [0, 1], bins = 50)
        histGlyph.data_source.data['y'] = bin_heights
        histGlyph.data_source.data['x_low'] = bin_edges[:-1]
        histGlyph.data_source.data['x_high'] = bin_edges[1:]
    
    return python_callback

go_but = Button(label="Convolve", button_type="success")
go_but.on_event(events.ButtonClick, convolve())
curdoc().add_root(column(row(p, pHist), go_but))
